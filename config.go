// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package main

import (
	"io/ioutil"

	"gitlab.com/studentennettwente/maildap/directory"
	"gitlab.com/studentennettwente/maildap/ldap"
	"gitlab.com/studentennettwente/maildap/mail"
	"gopkg.in/yaml.v2"
)

// Config contains the application configuration.
type Config struct {
	Directory  *directory.Config `yaml:"directory"`
	LDAP       *ldap.Config      `yaml:"ldap"`
	MailServer *mail.Config      `yaml:"mail_server"`
	LogLevel   string            `yaml:"log_level"`
}

// loadConfig loads configuration
func loadConfig(path string) (config *Config, err error) {
	config = new(Config)

	// Get configuration data
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return config, err
	}

	// Parse configuration
	err = yaml.Unmarshal(data, config)
	if err != nil {
		return
	}

	// Set default loglevel
	if config.LogLevel == "" {
		config.LogLevel = "info"
	}

	return
}
