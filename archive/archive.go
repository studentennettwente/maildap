// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

// Package archive implements a mailing list archive contained on disk.
// It is linked to an LDAP directory.
package archive

import (
	"io/ioutil"
	"path/filepath"
	"sync"

	"gitlab.com/studentennettwente/maildap/directory"
	"gitlab.com/studentennettwente/maildap/mail"
)

// Archive represents emails from disk.
type Archive struct {
	sync.RWMutex
	Lists     map[string]*List
	Directory *directory.Directory
	baseDir   string
}

// NewArchive creates a new (empty) archive.
// Ensures existence of the archive directory.
func NewArchive(dir string, directory *directory.Directory) (a *Archive, err error) {
	a = &Archive{
		Lists:     make(map[string]*List),
		Directory: directory,
		baseDir:   dir,
	}

	// Ensure directory exists
	err = ensureDirExists(dir)

	return
}

// LoadArchive loads an archive from disk.
func LoadArchive(dir string, directory *directory.Directory) (a *Archive, err error) {
	a, err = NewArchive(dir, directory)
	if err != nil {
		return
	}

	err = a.Load()
	return
}

// Load (re)loads the archive from disk.
func (a *Archive) Load() error {
	a.Lock()
	defer a.Unlock()

	// Get directories of mailing lists
	lists, err := ioutil.ReadDir(a.baseDir)
	if err != nil {
		return err
	}

	// Clear current index
	a.Lists = make(map[string]*List, len(lists))

	// Create and load all lists
	for _, l := range lists {
		if !l.IsDir() {
			continue
		}

		// Get corresponding group from LDAP directory,
		// or skip if it doesn't exist
		list := a.Directory.Group(l.Name())
		if list == nil {
			continue
		}

		// Load mailing list from disk
		a.Lists[list.ID], err = LoadList(list, filepath.Join(a.baseDir, l.Name()))
		if err != nil {
			return err
		}
	}

	return nil
}

// Add adds an email to the archive.
func (a *Archive) Add(msg *mail.Message) error {
	a.Lock()
	defer a.Unlock()

	for _, list := range msg.Lists {
		// Get mailing list for message
		l, err := a.getList(list)
		if err != nil {
			return err
		}

		// Add to archive
		err = l.Add(msg)
		if err != nil {
			return err
		}
	}

	return nil
}

// getList returns an archive list for a mailing list.
// The list is created if it doesn't exist.
func (a *Archive) getList(list *mail.List) (l *List, err error) {
	var ok bool
	l, ok = a.Lists[list.ID]
	if !ok {
		l, err = NewList(list, filepath.Join(a.baseDir, list.ID))
		if err != nil {
			return
		}
		a.Lists[list.ID] = l
	}

	return
}
