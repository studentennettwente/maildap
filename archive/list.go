// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package archive

import (
	"io/ioutil"
	"log"
	"path/filepath"
	"sync"

	"gitlab.com/studentennettwente/maildap/mail"
)

const (
	subjectMaxLength = 97
)

// List represents the archive of a single mailing list
type List struct {
	sync.RWMutex
	*mail.List
	Entries   map[string]Entry
	directory string
}

// NewList creates a new list archive for a mailing list.
// Ensures existence of the list archive directory.
func NewList(list *mail.List, dir string) (l *List, err error) {
	l = &List{
		List:      list,
		Entries:   make(map[string]Entry, 0),
		directory: dir,
	}

	// Ensure directory exists
	err = ensureDirExists(dir)

	return
}

// LoadList initialises a list from a directory.
func LoadList(list *mail.List, dir string) (l *List, err error) {
	// Create list
	l, err = NewList(list, dir)
	if err != nil {
		return
	}

	// Load from disk
	err = l.Load()

	return
}

// Load (re)loads the archive from disk.
func (l *List) Load() error {
	l.Lock()
	defer l.Unlock()

	// Get a list of files
	files, err := ioutil.ReadDir(l.directory)
	if err != nil {
		return err
	}

	// Ensure sufficient capacity
	l.Entries = make(map[string]Entry, len(files))

	// Read files and add to list
	for _, f := range files {
		if f.IsDir() {
			continue
		}

		file := filepath.Join(l.directory, f.Name())
		msg, err := mail.MessageFromFile(file, l.List)
		if err != nil {
			log.Printf("Error reading %q: %s", file, err)
			continue
		}

		l.add(msg, f.Name())
	}

	return nil
}

// Get retrieves a single message from the archive.
func (l *List) Get(id string) (msg *mail.Message, err error) {
	l.RLock()
	defer l.RUnlock()

	entry, ok := l.Entries[id]
	if !ok {
		return nil, nil
	}

	return mail.MessageFromFile(filepath.Join(l.directory, entry.File), l.List)
}

// Add a message to a list
func (l *List) Add(msg *mail.Message) (err error) {
	l.Lock()
	defer l.Unlock()

	// Store message on disk
	err = msg.WriteToFile(filepath.Join(l.directory, msg.FileName()))
	if err != nil {
		return
	}

	// Add message to the index
	l.add(msg, "")

	return
}

// add adds a message to the index
func (l *List) add(msg *mail.Message, fileName string) {
	if fileName == "" {
		fileName = msg.FileName()
	}

	// Get and limit subject to a sane length
	subject := msg.Header.Get("Subject")
	if len(subject) > subjectMaxLength {
		subject = subject[0:subjectMaxLength] + "..."
	}

	// Create entry
	entry := Entry{
		Subject: subject,
		Date:    msg.Date,
		From:    msg.Author,
		File:    fileName,
	}

	// Add to list
	l.Entries[msg.ID()] = entry
}
