// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package mail

import (
	"net/mail"
	"strings"
	"testing"
)

func TestSingleMessage_Write(t *testing.T) {
	t.Logf("Not yet implemented")
}

func TestSingleMessage_writeHeaders(t *testing.T) {
	t.Logf("Not yet implemented")
}

// Tests for headers
var headerTests = []struct{ In, Out string }{
	{"from: alice@example.com", "From: alice@example.com"},
	{"to: <test@example.com>", "To: Alice <alice@example.com>"},

	// Other headers
	{"mime-version: 1.0", "MIME-Version: 1.0"},

	// Mailing list headers that should be stripped
	{"Precedence: ignored", ""},
	{"X-Loop: ignored", ""},
	{"List-Id: ignored", ""},
	{"List-Archive: ignored", ""},
	{"List-Owner: ignored", ""},
	{"List-Help: ignored", ""},
	{"List-Subscribe: ignored", ""},
	{"List-Unsubscribe: ignored", ""},
	{"List-Post: ignored", ""},

	// Subject tests
	{"subject: test message", "Subject: [Test] test message"},
	{"subject: Subject", "Subject: [Test] Subject"},
	{"subject: [Test] Subject", "Subject: [Test] Subject"},
	{"subject: Re: [Test] Subject", "Subject: Re: [Test] Subject"},
	{"subject: RE: [Test] Subject", "Subject: RE: [Test] Subject"},
	{"subject: Fwd: [Test] Subject", "Subject: Fwd: [Test] Subject"},
	{"subject: FWD: [Test] Subject", "Subject: FWD: [Test] Subject"},
	{"subject: Re: [Other] Subject", "Subject: [Test] Re: [Other] Subject"},
}

func TestSingleMessage_writeMessageHeaders(t *testing.T) {
	// Test message
	msg := &SingleMessage{
		Message:   &Message{Message: new(mail.Message)},
		List:      testList,
		Recipient: testList.Members[0],
	}

	// Output buffer
	out := new(strings.Builder)

	// Test all headers
	for _, test := range headerTests {
		out.Reset()

		var err error
		msg.Message.Message, err = mail.ReadMessage(strings.NewReader(test.In + "\r\n\r\n"))
		if err != nil {
			t.Errorf("Error parsing header %q: %s", test.In, err)
			t.FailNow()
		}

		err = msg.writeMessageHeaders(out)
		if err != nil {
			t.Errorf("Error serializing header %q: %s", test.In, err)
			t.FailNow()
		}

		outStr := strings.TrimSpace(out.String())
		if outStr != test.Out {
			t.Errorf("Incorrect serialization for %q: expected %q, got %q",
				test.In, test.Out, outStr)
		}
	}
}
