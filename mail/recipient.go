// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package mail

import "fmt"

// Recipient contains an email recipient
type Recipient struct {
	Name, Address string
}

// String returns a string version of the recipient.
// It can be used in email headers.
func (r *Recipient) String() string {
	return fmt.Sprintf("%s <%s>", r.Name, r.Address)
}
