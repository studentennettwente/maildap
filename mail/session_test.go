// Copyright 2021 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package mail

import (
	"bytes"
	"strings"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
)

type testSession struct {
	Mail, Rcpt, Data          []string
	MailErr, RcptErr, DataErr []error
	Messages                  []*Message
}

var testTime = time.Date(2001, 02, 03, 04, 05, 0, 0, time.UTC)

var testBody = "Test 1, 2, 3."

var testSessions = []testSession{
	{
		Mail: []string{"<a@example.com>"},
		Rcpt: []string{"<b@example.com>"},
		Data: []string{
			"From: \"A\" <a@example.com>\r\n" +
				"To: \"B\" <b@example.com>\r\n" +
				"Date: " + testTime.Format(time.RFC1123Z) + "\r\n" +
				"Subject: Test\r\n\r\n" +
				testBody,
		},
		MailErr: []error{nil},
		RcptErr: []error{nil},
		DataErr: []error{nil},
		Messages: []*Message{
			{
				Message: nil,
				Lists:   []*List{testList},
				Author:  "<a@example.com>",
				Date:    testTime,
				Body:    []byte(testBody),
			},
		},
	},
	{
		Mail: []string{"<a@example.com>"},
		Rcpt: []string{"<b1@example.com>", "<b2@example.com>"},
		Data: []string{
			"From: \"A\" <a@example.com>\r\n" +
				"To: \"B\" <b@example.com>\r\n" +
				"Date: " + testTime.Format(time.RFC1123Z) + "\r\n" +
				"Subject: Multiple Recipient Test\r\n\r\n" +
				testBody,
		},
		MailErr: []error{nil},
		RcptErr: []error{nil, nil},
		DataErr: []error{nil},
		Messages: []*Message{
			{
				Message: nil,
				Lists:   []*List{testList, testList},
				Author:  "<a@example.com>",
				Date:    testTime,
				Body:    []byte(testBody),
			},
		},
	},
	{
		Mail: []string{"<a@example.com>"},
		Rcpt: []string{"<b@example.com>"},
		Data: []string{
			"X-Loop: <test@example.com>\r\n" +
				"From: \"A\" <a@example.com>\r\n" +
				"To: \"B\" <b@example.com>\r\n" +
				"Date: " + testTime.Format(time.RFC1123Z) + "\r\n" +
				"Subject: X-Loop Test\r\n\r\n" +
				testBody,
		},
		MailErr:  []error{nil},
		RcptErr:  []error{nil},
		DataErr:  []error{nil},
		Messages: nil,
	},
	{
		Mail: nil,
		Rcpt: nil,
		Data: nil,
	},
}

func init() {
	log.SetLevel(log.DebugLevel)
}

func TestSession(t *testing.T) {
	for _, ts := range testSessions {
		var msgs []*Message
		handler := func(m *Message) error {
			msgs = append(msgs, m)
			return nil
		}

		s := &Session{
			backend: &Backend{
				GetList: func(to string) *List {
					return testList
				},
				HandleFunc: (*MessageHandler)(&handler),
			},
		}

		for i, m := range ts.Mail {
			err := s.Mail(m)
			if err != ts.MailErr[i] {
				t.Errorf("MAIL FROM: expected error %q, got error %q", ts.MailErr[i], err)
				t.FailNow()
			}
		}

		for i, r := range ts.Rcpt {
			err := s.Rcpt(r)
			if err != ts.RcptErr[i] {
				t.Errorf("RCTP TO: expected error %q, got error %q", ts.RcptErr[i], err)
				t.FailNow()
			}
		}

		for i, d := range ts.Data {
			err := s.Data(strings.NewReader(d))
			if err != ts.DataErr[i] {
				t.Errorf("DATA: expected error %q, got error %q", ts.DataErr[i], err)
				t.FailNow()
			}
		}

		if len(ts.Messages) != len(msgs) {
			t.Errorf("Incorrect number of messages: expected %v, got %v",
				len(ts.Messages), len(msgs))
		}

		for i, exp := range ts.Messages {
			got := msgs[i]

			if len(exp.Lists) != len(got.Lists) {
				t.Errorf("Incorrect number of lists: expected %v, got %v",
					len(exp.Lists), len(got.Lists))
			}
			for i := range exp.Lists {
				if exp.Lists[i] != got.Lists[i] {
					t.Errorf("Incorrect message list: expected %q, got %q",
						exp.Lists[i].ID, got.Lists[i].ID)
				}
			}

			if exp.Author != got.Author {
				t.Errorf("Incorrect message author: expected %q, got %q",
					exp.Author, got.Author)
			}
			if exp.Date.Unix() != got.Date.Unix() {
				t.Errorf("Incorrect message date: expected %q, got %q",
					exp.Date, got.Date)
			}
			if !bytes.Equal(exp.Body, got.Body) {
				t.Errorf("Incorrect message body: expected %q, got %q",
					exp.Body, got.Body)
			}
		}
	}
}
