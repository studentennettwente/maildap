// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package mail

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"net/mail"
	"os"
	"time"

	"golang.org/x/crypto/blake2b"
)

// Message represents a single e-mail message on one or more mailing lists.
type Message struct {
	*mail.Message
	Lists  []*List
	Author string
	Date   time.Time
	Body   []byte
}

// MessageFromFile opens and parses a message from a file.
func MessageFromFile(path string, lists ...*List) (*Message, error) {
	// Create message
	m := &Message{Lists: lists}

	// Open file
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	// Read body from file
	err = m.SetBody(f)

	return m, err
}

// SingleMessages returns a set of single messages corresponding to this message.
func (m *Message) SingleMessages() (messages []*SingleMessage) {
	for _, list := range m.Lists {
		ms := make([]*SingleMessage, len(list.Members))
		for i, rcpt := range list.Members {
			ms[i] = &SingleMessage{
				Message:   m,
				List:      list,
				Recipient: rcpt,
			}
		}
		messages = append(messages, ms...)
	}
	return
}

// ID returns a unique identifier for this message
func (m *Message) ID() string {
	// Use blake2b for hashing
	hash, _ := blake2b.New(8, nil)

	// Hash the entire mail
	hash.Write([]byte(m.Header.Get("From")))
	hash.Write([]byte(m.Header.Get("To")))
	hash.Write([]byte(m.Header.Get("Subject")))
	hash.Write([]byte(m.Header.Get("Message-Id")))
	hash.Write([]byte(m.Date.String()))
	hash.Write(m.Body)

	// Return hex digest
	return hex.EncodeToString(hash.Sum(nil))
}

// FileName returns the filename for this message when stored in a maildir.
func (m *Message) FileName() string {
	// Get hostname of system
	host, err := os.Hostname()
	if err != nil {
		host = "localhost"
	}

	// Write message to file
	return fmt.Sprintf("%v.%s.%s", m.Date.Unix(), m.ID(), host)
}

// SetBody sets the body of the message from an io.Reader.
// Headers are modified as needed.
func (m *Message) SetBody(r io.Reader) (err error) {
	// Parse the message
	m.Message, err = mail.ReadMessage(r)
	if err != nil {
		return err
	}

	// Store the message body
	m.Body, err = ioutil.ReadAll(m.Message.Body)

	// Set author if not yet set
	if m.Author == "" {
		m.Author = m.Message.Header.Get("From")
	}

	// Set date if not yet set
	if m.Date.IsZero() {
		m.Date, _ = mail.ParseDate(m.Message.Header.Get("Date"))
	}

	return
}

// WriteRaw writes the current message to an io.Writer.
// Nothing is modified.
func (m *Message) WriteRaw(w io.Writer) (err error) {
	// Write headers
	if err = writeHeaders(w, m.Header); err != nil {
		return
	}

	// Final newline
	if _, err = w.Write([]byte("\n")); err != nil {
		return
	}

	// Write the message body
	_, err = io.Copy(w, bytes.NewReader(m.Body))

	return
}

// writeHeaders writes a set of headers to an io.Writer.
func writeHeaders(w io.Writer, headers mail.Header) error {
	for k, values := range headers {
		if err := writeHeader(w, k, values...); err != nil {
			return err
		}
	}
	return nil
}

// writeHeader writes a header to an io.Writer.
func writeHeader(w io.Writer, key string, values ...string) (err error) {
	for _, v := range values {
		_, err = w.Write([]byte(fmt.Sprintf("%s: %s\n", key, v)))
		if err != nil {
			return
		}
	}
	return
}

// WriteToFile writes the message to a given file.
func (m *Message) WriteToFile(path string) error {
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE, 0640)
	if err != nil {
		return err
	}
	defer f.Close()

	return m.WriteRaw(f)
}
