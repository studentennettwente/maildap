// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package mail

import (
	"fmt"
	"io"
	"strings"

	"github.com/emersion/go-smtp"
	log "github.com/sirupsen/logrus"
)

// Session contains the sessions information for a connection.
// It implements smtp.Session.
type Session struct {
	backend *Backend
	message *Message
}

// Mail handles the MAIL FROM command.
func (s *Session) Mail(from string) error {
	log.Debugf("Session %p - MAIL FROM %q", s, from)

	s.message = &Message{
		Author: from,
	}

	return nil
}

// Rcpt handles the RCPT TO command.
func (s *Session) Rcpt(to string) error {
	log.Debugf("Session %p - RCPT TO %q", s, to)

	// Check if list exists
	list := s.backend.GetList(to)
	if list == nil {
		log.Warnf("Unknown list for: %q", to)
		return smtp.ErrAuthRequired
	}

	// Add list to recipients
	s.message.Lists = append(s.message.Lists, list)

	return nil
}

// Data handles the DATA command,
// and the end of the message.
func (s *Session) Data(r io.Reader) error {
	log.Debugf("Session %p - DATA", s)

	// Ensure the MAIL FROM command was sent
	if s.message == nil {
		return temporaryError(fmt.Errorf("invalid"))
	}

	// Attempt to read the body, or return a temporary error
	err := s.message.SetBody(r)
	if err != nil {
		return temporaryError(err)
	}

	// Log the reception
	log.Infof("Received message on lists %s from %q with subject %q",
		strings.Join(listNames(s.message.Lists), ", "),
		s.message.Author,
		s.message.Header.Get("Subject"))

	// Loop detection
	for i, l := range s.message.Lists {
		if s.message.Header.Get("X-Loop") == fmt.Sprintf("<%s>", l.Address) {
			log.Warnf("Discard message to %q because of loop detection", l.Name)
			s.message.Lists = append(s.message.Lists[:i], s.message.Lists[i+1:]...)
			continue
		}
	}

	// Discard messages without recipients
	if len(s.message.Lists) == 0 {
		return nil
	}

	// Add the message(s) to send queue
	err = (*s.backend.HandleFunc)(s.message)
	if err != nil {
		return temporaryError(err)
	}

	return nil
}

// Reset discards the currently processed message.
func (s *Session) Reset() {
	log.Debugf("Session %p - reset", s)

	s.message = nil
}

// Logout frees all resources associated with session.
func (s *Session) Logout() error {
	log.Debugf("Session %p - logout", s)

	return nil
}

// listNames returns the names of a list of mailing lists.
func listNames(lists []*List) []string {
	names := make([]string, len(lists))
	for i, l := range lists {
		names[i] = l.Name
	}
	return names
}
