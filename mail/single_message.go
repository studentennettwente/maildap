// Copyright 2021 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package mail

import (
	"bytes"
	"fmt"
	"io"
	"regexp"
	"strings"
)

// SingleMessage is an email/list/recipient link.
// Recipient can be `nil`.
type SingleMessage struct {
	*Message
	List      *List
	Recipient *Recipient
}

// Write writes the current message to an io.Writer.
// The Recipient is used to modify headers as required.
func (m *SingleMessage) Write(w io.Writer) error {
	// Write headers
	if err := m.writeHeaders(w); err != nil {
		return err
	}

	// Write the message body
	_, err := io.Copy(w, bytes.NewReader(m.Body))

	return err
}

// writeHeaders writes the current message and list headers to an io.Writer.
// The Recipient is used to modify headers as required.
func (m *SingleMessage) writeHeaders(w io.Writer) (err error) {
	// Message headers
	err = m.writeMessageHeaders(w)
	if err != nil {
		return
	}

	// List headers
	err = writeHeaders(w, m.List.Header())
	if err != nil {
		return
	}

	// Final newline
	_, err = w.Write([]byte("\n"))

	return
}

// writeMessageHeaders writes the current message headers to an io.Writer.
// The Recipient is used to modify headers as required.
func (m *SingleMessage) writeMessageHeaders(w io.Writer) (err error) {
	// Message headers
	for k, values := range m.Header {
		var v string

		// Modify certain headers
		switch k {
		// Properly format subject
		case "Subject":
			values = []string{m.formattedSubject()}

		// Modify recipient if list is hidden
		case "To":
			if m.Recipient != nil && m.List.Hidden {
				v = fmt.Sprintf("%s <%s>", m.Recipient.Name, m.Recipient.Address)
			} else {
				v = m.Header.Get("To")
			}
			values = []string{v}

		// These are set from the mailing list config
		case "Precedence",
			"X-Loop",
			"List-Id",
			"List-Archive",
			"List-Owner",
			"List-Help",
			"List-Subscribe",
			"List-Unsubscribe",
			"List-Post":
			continue

		// While headers are technically case-insensitive,
		// Rspamd doesn't like it when the case is not 'correct'.
		case "Mime-Version":
			k = "MIME-Version"
		}

		// Write header
		if err = writeHeader(w, k, values...); err != nil {
			return
		}
	}

	return
}

// formattedSubject returns the message subject with a message tag.
// Existing message tags are removed under certain conditions.
func (m *SingleMessage) formattedSubject() string {
	subject := m.Header.Get("Subject")
	tag := `[` + m.List.Name + `]`

	// Check if subject starts with tag
	if strings.HasPrefix(subject, tag) {
		return subject
	}

	// Don't add a tag to replies or forwards for this list.
	matched, err := regexp.MatchString(`(?i)^(Re|Fwd): `+regexp.QuoteMeta(tag), subject)
	if err == nil && matched {
		return subject
	}

	return tag + ` ` + subject
}
