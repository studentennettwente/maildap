// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package mail

import (
	"strconv"
	"strings"

	"github.com/emersion/go-smtp"
)

// temporaryError creates a temporary SMTP error from
func temporaryError(err error) error {
	return &smtp.SMTPError{
		Code:         451,
		EnhancedCode: smtp.EnhancedCode{4, 0, 0},
		Message:      err.Error(),
	}
}

// parseSMTPError parses a received error into an smtp.SMTPError.
func parseSMTPError(err error) (*smtp.SMTPError, bool) {
	if e, ok := err.(*smtp.SMTPError); ok {
		return e, ok
	}

	parts := strings.SplitN(err.Error(), " ", 3)
	if len(parts) != 3 {
		return nil, false
	}

	e := &smtp.SMTPError{Message: parts[2]}
	e.Code, err = strconv.Atoi(parts[0])
	if err != nil {
		return nil, false
	}

	ehCode := strings.Split(parts[1], ".")
	if len(ehCode) != 3 {
		return nil, false
	}

	for i, c := range ehCode {
		e.EnhancedCode[i], err = strconv.Atoi(c)
		if err != nil {
			return nil, false
		}
	}

	return e, true
}
