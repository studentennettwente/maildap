// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package mail

// Config contains the configuration of an SMTP server.
type Config struct {
	// Address is the address to listen on.
	Address string `yaml:"address"`

	// Domain is the domain of this mail server.
	Domain string `yaml:"domain"`

	// SMTPServer contains the address the SMTP server to use for sending mail.
	SMTPServer string `yaml:"smtp_server"`

	// Directory is the directory used for storing received mails.
	Directory string `yaml:"directory"`
}

// ListRetriever represents a function that can be used to retrieve a mailing list.
type ListRetriever func(to string) *List
