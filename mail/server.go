// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

// Package mail implements a mail server, client and various other bits and bobs.
package mail

import (
	"errors"
	"sync"
	"time"

	"github.com/emersion/go-smtp"
)

const (
	// OutCapacity is the capacity of the output queue.
	OutCapacity = 250

	// OutCapacityMargin is the additional margin to keep in the output queue.
	// This margin avoids the output queue becoming blocked
	// when multiple messages arrive simultaneously.
	OutCapacityMargin = 50
)

// MessageHandler is a function that handles a message.
type MessageHandler func(*Message) error

// Server is an SMTP server.
type Server struct {
	*smtp.Server
	Client     *Client
	OutQueue   chan *SingleMessage
	HandleFunc MessageHandler
	waitGroup  sync.WaitGroup
}

// NewServer creates a new SMTP server.
func NewServer(config *Config, listFunc ListRetriever) (*Server, error) {
	// Create SMTP client for outgoing mails
	client, err := NewClient(config.SMTPServer)
	if err != nil {
		return nil, err
	}

	// Create server
	s := &Server{
		Client:   client,
		OutQueue: make(chan *SingleMessage, OutCapacity),
	}

	// Create basic SMTP server
	s.Server = smtp.NewServer(&Backend{HandleFunc: &s.HandleFunc, Directory: config.Directory, GetList: listFunc})
	s.Addr = config.Address
	s.Domain = config.Domain
	s.ReadTimeout = 30 * time.Second
	s.WriteTimeout = 30 * time.Second
	s.MaxMessageBytes = 104857600
	s.MaxRecipients = 10
	s.AuthDisabled = true

	// Default handler
	s.HandleFunc = s.HandleMessage

	return s, nil
}

// ListenAndServe listens on the network address s.Addr and serves incoming connections.
// Incoming messages are automatically added to OutQueue.
func (s *Server) ListenAndServe() error {
	// Perform shutdown on function exit
	defer s.Shutdown()

	// Start the server
	return s.Server.ListenAndServe()
}

// Shutdown performs a graceful shutdown of the mail server.
func (s *Server) Shutdown() {
	// Close the output queue and wait for tasks to be completed
	close(s.OutQueue)
	s.waitGroup.Wait()
}

// HandleMessage handles an incoming message from the queue or some external component.
func (s *Server) HandleMessage(in *Message) error {
	messages := in.SingleMessages()

	// Check for sufficient capacity
	if len(s.OutQueue)+len(messages)+OutCapacityMargin > OutCapacity {
		return errors.New("server capacity exceeded")
	}

	// Add mails to output queue
	for _, out := range messages {
		s.OutQueue <- out
	}
	return nil
}

// OutWorker takes messages from OutQueue and attempts to send them.
func (s *Server) OutWorker() error {
	s.waitGroup.Add(1)
	defer s.waitGroup.Done()

	return s.Client.Worker(s.OutQueue)
}
