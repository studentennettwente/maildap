// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

// Package directory implements an LDAP directory.
package directory

import (
	"fmt"
	"net/url"
	"path"
	"strings"
	"sync"

	"gitlab.com/studentennettwente/maildap/ldap"
	"gitlab.com/studentennettwente/maildap/mail"
)

// Directory represents an LDAP backed directory that maps users and groups
// to email recipients and mailing lists respectively.
// Directory is thread-safe.
type Directory struct {
	sync.RWMutex
	Config *Config
	groups map[string]*mail.List
	users  map[string]*mail.Recipient
	client *ldap.Client
}

// New returns initialises and returns a Directory.
func New(config *Config, ldapConfig *ldap.Config) (d *Directory, err error) {
	d = &Directory{Config: config}
	d.client, err = ldap.NewClient(ldapConfig)
	if err != nil {
		return
	}
	err = d.Refresh()
	return
}

// User retrieves the email recipient information for a user.
func (d *Directory) User(id string) *mail.Recipient {
	d.RLock()
	defer d.RUnlock()

	if r, ok := d.users[strings.ToLower(id)]; ok {
		return r
	}

	return nil
}

// Group retrieves a mailing list for a group ID.
func (d *Directory) Group(id string) *mail.List {
	d.RLock()
	defer d.RUnlock()

	if l, ok := d.groups[strings.ToLower(id)]; ok {
		return l
	}

	return nil
}

// List retrieves a mailing list for a list address.
func (d *Directory) List(addr string) *mail.List {
	parts := strings.SplitN(addr, "@", 2)
	if len(parts) == 0 {
		return nil
	}

	return d.Group(parts[0])
}

// Connect (re)connects to the LDAP server.
func (d *Directory) Connect() error {
	return d.client.Connect()
}

// Close closes the LDAP connection.
func (d *Directory) Close() {
	d.client.Close()
	d.client.Conn = nil
}

// Refresh reloads the cache for the Directory.
func (d *Directory) Refresh() error {
	d.Lock()
	defer d.Unlock()

	// Reconnect if there is no connection
	if d.client.Conn == nil {
		err := d.Connect()
		if err != nil {
			return err
		}
	}

	if err := d.refreshUsers(); err != nil {
		return err
	}
	if err := d.refreshGroups(); err != nil {
		return err
	}

	lists := make([]string, 0, len(d.groups))
	for _, l := range d.groups {
		lists = append(lists, l.Name)
	}

	return nil
}

// refreshUsers reloads the users from the LDAP server.
func (d *Directory) refreshUsers() error {
	users, err := d.client.Users()
	if err != nil {
		return err
	}

	d.users = make(map[string]*mail.Recipient, len(users))
	for _, u := range users {
		if len(u.Email) == 0 {
			continue
		}

		d.users[strings.ToLower(u.ID)] = &mail.Recipient{
			Name:    u.Name,
			Address: u.Email[0],
		}
	}

	return nil
}

// refreshGroups reloads the groups from the LDAP server.
func (d *Directory) refreshGroups() error {
	groups, err := d.client.Groups()
	if err != nil {
		return err
	}

	d.groups = make(map[string]*mail.List, len(groups))
	for _, g := range groups {
		// Skip empty groups and user groups
		if len(g.Members) == 0 || len(g.Members) == 1 && g.Members[0] == g.Name {
			continue
		}

		// Get recipients for group members
		recips := make([]*mail.Recipient, 0, len(g.Members))
		for _, m := range g.Members {
			if r, ok := d.users[m]; ok {
				recips = append(recips, r)
			}
		}

		// Create mailing list
		d.addGroup(g.Name, recips, false)
	}

	// If a list name for all members of groups has been configured, add it
	if d.Config.MemberList != "" {
		// Create a deduplicated map of members
		memberMap := make(map[*mail.Recipient]struct{}, len(d.users))
		for _, g := range d.groups {
			for _, u := range g.Members {
				memberMap[u] = struct{}{}
			}
		}

		// Convert to a member list
		allMembers := make([]*mail.Recipient, 0, len(memberMap))
		for u := range memberMap {
			allMembers = append(allMembers, u)
		}

		d.addGroup(d.Config.MemberList, allMembers, true)
	}

	// If a list name for all user accounts has been configured, add it.
	if d.Config.AllList != "" {
		allUsers := make([]*mail.Recipient, 0, len(d.users))
		for _, u := range d.users {
			allUsers = append(allUsers, u)
		}

		d.addGroup(d.Config.AllList, allUsers, true)
	}

	return nil
}

// addGroup adds a group to the directory.
func (d *Directory) addGroup(name string, members []*mail.Recipient, hidden bool) {
	id := strings.ToLower(name)
	d.groups[id] = &mail.List{
		ID:      id,
		Name:    name,
		Address: fmt.Sprintf("%s@%s", id, d.Config.Domain),
		URL:     urlJoin(d.Config.URL, id),
		Members: members,
		Hidden:  hidden,
	}
}

// urlJoin joins URL parts.
func urlJoin(elem ...string) string {
	if len(elem) == 0 {
		return ""
	}

	u, err := url.Parse(elem[0])
	if err != nil {
		return path.Join(elem...)
	}

	elem[0] = u.Path
	u.Path = path.Join(elem...)
	return u.String()
}
