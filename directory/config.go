// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package directory

// Config represents the Directory configuration.
type Config struct {
	// Domain contains the domain name of the group addresses from LDAP.
	Domain string `yaml:"domain"`

	// AllLists contains the name of a list containing all users.
	// This is disabled when AllLists is empty.
	AllList string `yaml:"all_list"`

	// MemberLists contains the name of a list containing all members of groups.
	// This is disabled when MemberLists is empty.
	MemberList string `yaml:"member_list"`

	// URL contains the url to the web-interface of the mailing system.
	// This is appended by the mailing list ID to create links to the list archive.
	URL string `yaml:"url"`
}
