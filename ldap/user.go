// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package ldap

import (
	"strings"

	"github.com/go-ldap/ldap"
)

// User represents an LDAP user.
type User struct {
	DN, ID, Name string
	Email        []string
}

// NewUser creates a User from an LDAP entry.
func NewUser(entry *ldap.Entry) (*User, error) {
	u := &User{DN: entry.DN}
	for _, attr := range entry.Attributes {
		switch strings.ToLower(attr.Name) {
		case "uid":
			u.ID = attr.Values[0]
		case "cn":
			u.Name = attr.Values[0]
		case "mail":
			u.Email = attr.Values
		}
	}

	return u, nil
}
