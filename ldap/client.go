// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

// Package ldap implements an LDAP client.
package ldap

import (
	"crypto/tls"
	"net/url"

	"github.com/go-ldap/ldap"
)

const (
	groupFilter = "(&(objectClass=posixGroup))"
	userFilter  = "(&(objectClass=posixAccount))"
)

// Client represents an LDAP server connection.
// It is an extended version of an LDAP connection from go-ldap.
type Client struct {
	*ldap.Conn
	config *Config
}

// NewClient creates and connects to a server using a username and password.
func NewClient(config *Config) (c *Client, err error) {
	c = &Client{
		config: config,
	}

	return c, c.Connect()
}

// Connect connects to the server.
func (c *Client) Connect() (err error) {
	// Ensure any existing connection is closed.
	if c.Conn != nil {
		c.Conn.Close()
	}

	// Connect to server
	c.Conn, err = ldap.DialURL(c.config.URL)
	if err != nil {
		return
	}

	// Upgrade using StartTLS if required
	if c.config.StartTLS {
		host, _, _ := parseURL(c.config.URL)
		err = c.StartTLS(&tls.Config{
			ServerName: host,
		})
		if err != nil {
			return
		}
	}

	// Login using username/password
	if c.config.Password != "" {
		err = c.Bind(c.config.Username, c.config.Password)
	} else {
		err = c.UnauthenticatedBind(c.config.Username)
	}

	return
}

// parseURL parses an LDAP url
func parseURL(addr string) (host, port string, err error) {
	var lurl *url.URL

	lurl, err = url.Parse(addr)
	if err != nil {
		return
	}

	return lurl.Hostname(), lurl.Port(), nil
}

// simpleSearch is a version of search with some sane defaults for the search request.
func (c *Client) simpleSearch(base, filter string, attrs []string) (*ldap.SearchResult, error) {
	return c.Search(ldap.NewSearchRequest(
		base,
		ldap.ScopeWholeSubtree,
		ldap.NeverDerefAliases,
		0, 0, false,
		filter,
		attrs,
		nil,
	))
}

// Users returns a list of users present on the LDAP server
func (c *Client) Users() ([]*User, error) {
	result, err := c.simpleSearch(c.config.UserBase, userFilter, []string{"uid", "cn", "mail"})
	if err != nil {
		return nil, err
	}

	users := make([]*User, 0, len(result.Entries))
	for _, entry := range result.Entries {
		user, err := NewUser(entry)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}

	return users, nil
}

// Groups returns a list of groups present on the LDAP server
func (c *Client) Groups() ([]*Group, error) {
	result, err := c.simpleSearch(c.config.GroupBase, groupFilter, []string{"dn", "cn", "memberuid"})
	if err != nil {
		return nil, err
	}

	groups := make([]*Group, 0, len(result.Entries))
	for _, entry := range result.Entries {
		group, err := NewGroup(entry)
		if err != nil {
			return nil, err
		}
		groups = append(groups, group)
	}

	return groups, nil
}
