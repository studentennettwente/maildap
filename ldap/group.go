// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package ldap

import (
	"strings"

	"github.com/go-ldap/ldap"
)

// Group represents an LDAP group.
type Group struct {
	DN, Name string
	Members  []string
}

// NewGroup creates a Group from an LDAP entry.
func NewGroup(entry *ldap.Entry) (*Group, error) {
	g := &Group{DN: entry.DN}
	for _, attr := range entry.Attributes {
		switch strings.ToLower(attr.Name) {
		case "cn":
			g.Name = attr.Values[0]
		case "memberuid":
			g.Members = attr.Values
		}
	}

	return g, nil
}
