module gitlab.com/studentennettwente/maildap

go 1.12

require (
	github.com/emersion/go-smtp v0.11.1
	github.com/go-ldap/ldap v3.0.3+incompatible
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/crypto v0.0.0-20190617133340-57b3e21c3d56
	gopkg.in/asn1-ber.v1 v1.0.0-20181015200546-f715ec2f112d // indirect
	gopkg.in/yaml.v2 v2.2.2
)
