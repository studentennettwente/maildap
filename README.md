# MaiLDAP
[![godoc](https://godoc.org/gitlab.com/studentennettwente/maildap?status.svg)](https://godoc.org/gitlab.com/studentennettwente/maildap)
[![pipeline status](https://gitlab.com/studentennettwente/maildap/badges/master/pipeline.svg)](https://gitlab.com/studentennettwente/maildap/commits/master)
[![coverage report](https://gitlab.com/studentennettwente/maildap/badges/master/coverage.svg)](https://gitlab.com/studentennettwente/maildap/commits/master)

An LDAP-backed mailing list server.
See `config.dist.yaml` for example configuration.

## Install

Download and install the program in your Go directory using:

```
go get gitlab.com/studentennettwente/maildap
```

Or download one of the latest builds:

- [Linux AMD64](https://gitlab.com/studentennettwente/maildap/-/jobs/artifacts/master/browse?job=build:linux:amd64)
