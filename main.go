// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

// MaiLDAP is an LDAP-backed mailing list server.
package main

import (
	"flag"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/studentennettwente/maildap/archive"
	"gitlab.com/studentennettwente/maildap/directory"
	"gitlab.com/studentennettwente/maildap/mail"
)

const (
	cacheDuration = 300 * time.Second
	retryDuration = 10 * time.Second
)

// cacheHandler handles cache refreshes
func cacheHandler(d *directory.Directory) {
	ticker := time.NewTicker(cacheDuration)
	defer ticker.Stop()

	for range ticker.C {
		log.Printf("Refreshing directory cache...")
		err := d.Refresh()
		if err != nil {
			log.Printf("Error refreshing directory cache: %s", err)
			d.Close()
			continue
		}

		log.Print("Successfully refreshed cache!")
	}
}

// outHandler handles outgoing email
func outHandler(s *mail.Server) {
	for {
		err := s.OutWorker()

		// Return on clean exit
		if err == nil {
			return
		}

		// Some error has occurred, retry in a bit
		log.Printf("Error sending mail: %s", err)
		time.Sleep(retryDuration)
	}
}

func main() {
	var configFile string

	flag.StringVar(&configFile, "config", "config.yaml", "Configuration file")
	flag.Parse()

	// Load configuration
	config, err := loadConfig(configFile)
	if err != nil {
		log.Fatalf("Error loading config file %s: %s", configFile, err)
	}

	// Set loglevel
	level, err := log.ParseLevel(config.LogLevel)
	if err != nil {
		log.Fatalf("Invalid log level: %q", config.LogLevel)
	}
	log.SetLevel(level)

	// Create list directory
	d, err := directory.New(config.Directory, config.LDAP)
	if err != nil {
		log.Fatalf("Error connecting to LDAP: %s", err)
	}
	defer d.Close()

	// Load on-disk archive
	log.Printf("Loading mails from archive, this may take some time...")
	arch, err := archive.LoadArchive(config.MailServer.Directory, d)
	if err != nil {
		log.Fatalf("Error reading mail archive: %s", err)
	}

	// Ensure the cache is refreshed regularly
	go cacheHandler(d)

	// Create mail server
	ms, err := mail.NewServer(config.MailServer, d.List)
	if err != nil {
		log.Fatalf("Error creating SMTP server: %s", err)
	}
	defer ms.Shutdown()

	// Setup archive and mail handlers
	ms.HandleFunc = func(message *mail.Message) error {
		if err := ms.HandleMessage(message); err != nil {
			return err
		}
		return arch.Add(message)
	}

	// Ensure mail goes out
	go outHandler(ms)

	// Start the mail server
	log.Printf("Listening on %s", config.MailServer.Address)
	log.Fatal(ms.ListenAndServe())
}
